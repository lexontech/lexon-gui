window.lexon_blocks.forEach(block => {
    Blockly.Blocks[block.type] = {
        init: function() {
            this.jsonInit(block);
        }
    };
});

const workspace = Blockly.inject('blocklyDiv',{
    toolbox: document.getElementById('toolbox')
});

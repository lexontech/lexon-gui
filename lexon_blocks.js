// Generated from https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#hhhvxp
window.lexon_blocks = [{
  "type": "lexon_article",
  "message0": "ARTICLE:  %1",
  "args0": [
    {
      "type": "field_input",
      "name": "NAME",
      "text": ""
    }
  ],
  "inputsInline": true,
  "nextStatement": [
    "lexon_chapter",
    "lexon_preamble"
  ],
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "lexon_comment",
  "message0": "COMMENT:  %1",
  "args0": [
    {
      "type": "field_input",
      "name": "NAME",
      "text": ""
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "lexon_preamble",
  "message0": "PREAMBLE: %1",
  "args0": [
    {
      "type": "input_statement",
      "name": "NAME"
    }
  ],
  "previousStatement": [
    "lexon_preamble",
    "lexon_chapter"
  ],
  "nextStatement": "lexon_chapter",
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "lexon_chapter",
  "message0": "CHAPTER: %1 %2 %3",
  "args0": [
    {
      "type": "field_input",
      "name": "CHAPTER_NAME",
      "text": ""
    },
    {
      "type": "input_dummy"
    },
    {
      "type": "input_statement",
      "name": "STATEMENT"
    }
  ],
  "previousStatement": [
    "lexon_chapter",
    "lexon_preamble"
  ],
  "nextStatement": "lexon_chapter",
  "colour": 230,
  "tooltip": "",
  "helpUrl": ""
}]
